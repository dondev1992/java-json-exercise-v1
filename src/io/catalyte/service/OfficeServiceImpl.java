package io.catalyte.service;

import io.catalyte.dao.OfficeDao;
import io.catalyte.entity.Office;

import java.io.IOException;
import java.util.List;

public class OfficeServiceImpl implements OfficeService{

    public OfficeDao officeDao;

    public void setOfficeDao(OfficeDao officeDao) {
        this.officeDao = officeDao;
    }

    @Override
    public List<Office> getOffices() throws IOException {
        return officeDao.getOffices();

    }
}

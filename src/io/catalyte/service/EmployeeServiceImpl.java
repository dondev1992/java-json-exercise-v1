package io.catalyte.service;

import io.catalyte.dao.EmployeeDao;
import io.catalyte.entity.Employee;

import java.io.IOException;
import java.util.List;

public class EmployeeServiceImpl implements EmployeeService{
    public EmployeeDao employeeDao;
    public EmployeeService employeeService;

    public void setEmployeeDao(EmployeeDao employeeDao) {
        this.employeeDao = employeeDao;
    }

    @Override
    public List<Employee> getEmployees() throws IOException {

        return employeeDao.getEmployees();
    }
}

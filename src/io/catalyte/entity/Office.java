package io.catalyte.entity;

import java.util.Date;

public class Office {
    public String name;
    public String street;
    public String city;
    public String state;
    public int zip;
    public Date date;

    public Date getDate() {
        return date;
    }
}

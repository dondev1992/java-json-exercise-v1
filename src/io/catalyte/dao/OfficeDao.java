package io.catalyte.dao;

import io.catalyte.entity.Office;

import java.io.IOException;
import java.util.List;

public interface OfficeDao {

    List<Office> getOffices() throws IOException;
}

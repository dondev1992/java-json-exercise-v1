import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter;
import io.catalyte.controller.EmployeeController;
import io.catalyte.controller.OfficeController;
import io.catalyte.dao.EmployeeCsvData;
import io.catalyte.dao.EmployeeXmlData;
import io.catalyte.dao.OfficeData;
import io.catalyte.entity.Employee;
import io.catalyte.entity.Office;
import io.catalyte.service.EmployeeServiceImpl;
import io.catalyte.service.OfficeServiceImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ApplicationRunner {
    public static void main(String[] args) throws IOException {

        EmployeeController employeeController = new EmployeeController();
        EmployeeServiceImpl employeeServiceImpl = new EmployeeServiceImpl();

        EmployeeCsvData employeeCsvData = new EmployeeCsvData();

        employeeController.setEmployeeService(employeeServiceImpl);
        employeeServiceImpl.setEmployeeDao(employeeCsvData);

        List<Employee> employeeList = employeeController.getEmployees();

        employeeList.forEach(employee -> System.out.println("Employee Name: " + employee.FirstName + " " + employee.LastName + "\n" +
                "Hire Date: " + employee.HireDate + "\n\n"));

        // initialize json adapter
        Moshi moshi = new Moshi.Builder()
                .add(Date.class, new Rfc3339DateJsonAdapter())
                .build();
//        JsonAdapter<Employee> jsonEmployee = moshi.adapter(Employee.class);


        OfficeController officeController = new OfficeController();
        OfficeServiceImpl officeServiceImpl = new OfficeServiceImpl();
        OfficeData officeData = new OfficeData();


        officeController.setOfficeService(officeServiceImpl);
        officeServiceImpl.setOfficeDao(officeData);

        List<Office> officeList = officeController.getOffices();

//        JsonAdapter<Office> jsonOffice = moshi.adapter(Office.class);

        // loop through offices and write each out to the console
        officeList.forEach(office -> System.out.println("Office: " + office.name + "\n" + "Address: " + office.street + ", " + office.city + ", " +
                office.state + " " + office.zip + "\n" + "Established: " + office.date + "\n\n"));




    }
}